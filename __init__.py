#This file is part product_barcode module for Tryton.
#The COPYRIGHT file at the top level of this repository contains
#the full copyright notices and license terms.

from trytond.pool import Pool
from . import product


def register():
    Pool.register(
        product.AssetLocation,
        product.Template,
        module='product_asset_attribute', type_='model')
    Pool.register(
        product.AssetInventory,
        module='product_asset_attribute', type_='report')
