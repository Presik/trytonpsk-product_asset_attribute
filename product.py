# This file is part of Tryton.  The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.

from trytond.model import fields, ModelSQL, ModelView
from trytond.pool import Pool, PoolMeta
from trytond.pyson import Eval
from trytond.report import Report
from trytond.transaction import Transaction

STATES = {
    'readonly': ~Eval('active', True),
}


class AssetLocation(ModelSQL, ModelView):
    "Asset Location"
    __name__ = "product.asset_location"
    name = fields.Char('Name', required=True)


class Template(metaclass=PoolMeta):
    __name__ = 'product.template'
    model_num = fields.Char('Model Number', states=STATES)
    external_code = fields.Char('External Code', states=STATES)
    serial = fields.Char('Serial', states=STATES)
    asset_location = fields.Many2One('product.asset_location', 'Asset Location',
        states=STATES)
    owner = fields.Many2One('party.party', 'Ower', select=True,
         states=STATES)


class AssetInventory(Report):
    'Asset Inventory'
    __name__ = 'product.asset_inventory'

    @classmethod
    def get_context(cls, records, header, data):
        report_context = super().get_context(records, header, data)
        pool = Pool()
        Company = pool.get('company.company')
        Template = pool.get('product.template')
        company_id = Transaction().context.get('company')
        report_context['company'] = Company(company_id)
        report_context['records'] = Template.search([
            ('type', '=', 'assets'),
        ])
        return report_context
